import * as Sentry from '@sentry/react';
import { apiResponse } from "./api-response";

export const apiService = async ({
    method = "",
    path = "/", 
    token = null,
    body = null
}) => {
    var headers;
    var fetchInput;
    var obj = apiResponse();

    if (token) {
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        };
    } else {
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
    };

    if (body) {
        fetchInput = {
            method: method,
            headers: headers,
            body: JSON.stringify(body)
        };
    } else {
        fetchInput = {
            method: method,
            headers: headers
        };
    }

    try {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}${path}`, fetchInput);
        const resJson = res.json();
        obj.resData = resJson?.data;
        obj.resStatus = resJson.status;
        return obj;

    } catch (error) {
        // if (error.response.status === 401) {
        //   _.throttle(() => {
        //     onLogout()
        //   }, 3000)()
        // }
        Sentry.withScope(function (scope) {
          scope.setTransactionName('REACT FETCH ' + method);
          Sentry.captureException(error);
        })
        obj.resData = error.response.data;
        obj.resStatus = error.response.status;
        return obj;
    };
};