import './App.css';
import { BrowserRouter} from 'react-router-dom';
import Router from './routes';
import { QueryClient, QueryClientProvider } from 'react-query';
import RouterWithGrowthbook from './growthbook';

function App() {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        {/* <Router/> */}
        <RouterWithGrowthbook/>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;
