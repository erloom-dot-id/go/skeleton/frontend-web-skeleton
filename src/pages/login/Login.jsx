import React, { useState } from 'react';
import { LoginPage } from '../../components/pages/LoginPage';
import { apiService } from '../../api';
import { useQuery } from 'react-query';
import { useAuthStore } from '../../store';
import { useNavigate } from 'react-router';
import { useFeature } from '@growthbook/growthbook-react';

const Login = () => {
    const { token } = useAuthStore((state) => state);
    const setTokenFunction = useAuthStore((state) => state.setTokenFunction);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [submitClicked, setSubmitClicked] = useState(false);
    const navigate = useNavigate();
    const forgotPasswordEnabled = useFeature("simple-project-fe.forgot-password");

    useQuery({
        queryKey: ['login'],
        refetchOnWindowFocus: false,
        queryFn: async () => {
            const res = await apiService({
                method: "POST",
                path: "/login",
                body: {
                    "email": email,
                    "password": password,
                }
            });
            return res;
        },
        onSuccess: (res) => {
            if (res?.resStatus !== 200) {
                alert("Failed Login");
            } else {
                setTokenFunction(res?.resData?.data?.access_token);
                alert("Success to Login with this token :" + token);
            }
        },
        onError: (res) => {
            alert("Failed fetch to server");
        },
        enabled: submitClicked
    });

    const loginHandler = () => {
        setSubmitClicked(true);
        navigate("/list-user");
    };

    const forgotPasswordHandler = () => {
        if (forgotPasswordEnabled.on) {
            console.log("Forgot Password has Clicked");
        } else {
            alert("Forgot Password has Maintenance");
        }
    };

    return (
        <>
            <LoginPage 
                inputEmailHandler={e => setEmail(e.target.value)}
                inputPasswordHandler={e => setPassword(e.target.value)}
                forgotPasswordHandler={forgotPasswordHandler}
                submitHandler={loginHandler}
            />
        </>
    );
};

export default Login;