import React from 'react';

const ListUser = () => {
    return (
        <>
            <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
                <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                    <img
                        className="mx-auto h-10 w-auto"
                        src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                        alt="Your Company"
                    />
                    <table className="border-collapse border border-slate-400 mt-10 w-full">
                        <thead>
                            <tr>
                                <th className="border border-slate-300">No.</th>
                                <th className="border border-slate-300">Nama</th>
                                <th className="border border-slate-300">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="border border-slate-300 text-center">1</td>
                                <td className="border border-slate-300 text-center">User 1</td>
                                <td className="border border-slate-300 text-center">user1@mail.com</td>
                            </tr>
                            <tr>
                                <td className="border border-slate-300 text-center">2</td>
                                <td className="border border-slate-300 text-center">User 2</td>
                                <td className="border border-slate-300 text-center">user2@mail.com</td>
                            </tr>
                            <tr>
                                <td className="border border-slate-300 text-center">3</td>
                                <td className="border border-slate-300 text-center">User 3</td>
                                <td className="border border-slate-300 text-center">user3@mail.com</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};

export default ListUser;