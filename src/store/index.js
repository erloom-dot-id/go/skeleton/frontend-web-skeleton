import { create } from "zustand";
import { createWithEqualityFn } from 'zustand/traditional';
import { createJSONStorage, persist } from 'zustand/middleware';
import AsyncStorage from "@react-native-async-storage/async-storage";

export const useUserWithPersistStore = createWithEqualityFn(
    persist(
        (set) => ({
            user: [],
            setUserFunction: (payload) => {
                set({user: payload});
            }
        }),
        {
            name: 'user-storage', // UNIQUE NAME
            storage: createJSONStorage(() => AsyncStorage),
        }
    )
);


export const useAuthStore = create((set) => ({
    token: null,
    setTokenFunction: (payload) => {
        set({ token: payload });
    },
}));

export const useUserStore = create((set) => ({
    user: [],
    setUserFunction: (payload) => {
        set({ user: payload });
    },
}));