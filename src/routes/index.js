import React from 'react';
import {Routes, Route} from 'react-router-dom';
import Login from '../pages/login/Login';
import ListUser from '../pages/user/ListUser';

const Router = () => {
    return (
        <Routes>
            <Route path="/" Component={Login}/>
            <Route path="/list-user" Component={ListUser}/>
        </Routes>
    );
};

export default Router;