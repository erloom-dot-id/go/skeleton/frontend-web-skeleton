import { Input } from "../atoms/Input";
import { Typography, medium, sm } from "../atoms/Typography";

export const InputForm = ({
    label = "Input Label", 
    placeholder = "",
    id = "",
    name = "",
    type = "text",
    changeHandler = (e) => {
        console.log(e.target.value);
    },
}) => {
    return (
        <>
            <label htmlFor={id} className="block mb-2">
                <Typography 
                    text={label} 
                    weight={medium} 
                    size={sm} 
                    className="leading-6 text-gray-900"
                />
            </label>
            <Input 
                id={id} 
                name={name} 
                type={type} 
                placeholder={placeholder}
                changeHandler={changeHandler}
            />
        </>

    );
};