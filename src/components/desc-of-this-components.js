// Flow dari component ini menggunakan metodologi Atomic Design.

// 1. Atoms
// Contoh: buttons, form inputs, typography

// 2. Molecules
// Gabungan dari beberapa atom.
// Contoh: input search, input box, labels

// 3. Organisms
// Gabungan dari molecules dan juga atom.
// Contoh: header, footer, form

// 4. Templates
// Tampilan konten dari sebuah page yang dapat digunakan di page lain.
// Contoh: sidebar, detail produk, form tambah/update produk

// 5. Pages
// Full desain untuk sebuah page.
// Contoh: login page, home page, dashboard page