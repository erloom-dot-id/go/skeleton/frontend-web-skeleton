import { LoginFormTemplate } from "../templates/LoginFormTemplate";

export const LoginPage = ({
    inputEmailHandler = (e) => {
        console.log(e.target.value);
    },
    inputPasswordHandler = (e) => {
        console.log(e.target.value);
    },
    forgotPasswordHandler = () => {
        console.log("Forgot Password has Clicked");
    },
    submitHandler = () => {
        console.log("Submit has Clicked");
    }
}) => {
    return (
        <>
            <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
                <LoginFormTemplate
                    inputEmailHandler={inputEmailHandler}
                    inputPasswordHandler={inputPasswordHandler}
                    forgotPasswordHandler={forgotPasswordHandler}
                    submitHandler={submitHandler}
                />
            </div>
        </>
    );
};