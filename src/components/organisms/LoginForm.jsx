import { Button } from "../atoms/Button";
import { ButtonLink } from "../atoms/ButtonLink";
import { Input } from "../atoms/Input";
import { Typography, medium, sm } from "../atoms/Typography";
import { InputForm } from "../molecules/InputForm";

export const LoginForm = ({
    inputEmailHandler = (e) => {
        console.log(e.target.value);
    },
    inputPasswordHandler = (e) => {
        console.log(e.target.value)
    },
    forgotPasswordHandler = () => {
        console.log("Forgot Password has Clicked");
    },
    submitHandler = () => {
        console.log("Sign In has Clicked");
    }
}) => {
    return (
        <>
            <div className="space-y-6">
                <div>
                    <InputForm
                        label="Email address"
                        id="email"
                        name="email"
                        type="email"
                        changeHandler={inputEmailHandler}
                    />
                </div>
                <div>
                    <div className="flex items-center justify-between">
                        <label htmlFor="password" className="block mb-2">
                            <Typography
                                text="Password" 
                                weight={medium} 
                                size={sm} 
                                className="leading-6 text-gray-900"
                            />
                        </label>
                        <div>
                            <ButtonLink label="Forgot password?" clickHandler={forgotPasswordHandler}/>
                        </div>
                    </div>
                    <Input
                        id="password" 
                        name="password"
                        type="password"
                        changeHandler={inputPasswordHandler}
                    />
                </div>
                <div>
                    <Button
                        label="Sign In"
                        isSubmit={true}
                        clickHandler={submitHandler}
                    />
                </div>
          </div>
        </>
    );
};