export const ButtonLink = ({
    label = "Link", 
    textColor = "",
    textWeight = "",
    className = "",
    clickHandler = () => {
        console.log("Clicked")
    }
}) => {
    var txtColor = textColor !== "" ? textColor : "text-indigo-600 hover:text-indigo-500";
    var txtWeight = textWeight !== "" ? textWeight : "font-semibold";

    var linkStyle = [
        "text-sm leading-6",
        txtColor,
        txtWeight,
        className,
    ].join(" ");

    return (
        <>
            <button className={linkStyle} onClick={clickHandler}>
                {label}
            </button>
        </>
    );
};