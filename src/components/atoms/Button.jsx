export const Button = ({
    label = "Button", 
    backgroundColor = "",
    textColor = "",
    textWeight = "",
    className = "",
    isSubmit = false,
    clickHandler = () => {
        console.log("Clicked")
    }
}) => {
    var bgColor = backgroundColor !== "" ? backgroundColor : "bg-indigo-600 hover:bg-indigo-500";
    var txtColor = textColor !== "" ? textColor : "text-white";
    var txtWeight = textWeight !== "" ? textWeight : "font-semibold";

    var btnStyle = [
        "flex w-full justify-center rounded-md px-3 py-1.5 shadow-sm",
        "text-sm leading-6",
        "focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600",
        txtColor,
        txtWeight,
        bgColor,
        className,
    ].join(" ");

    return (
        <>
            <button type={isSubmit ? "submit" : "button"} className={btnStyle} onClick={clickHandler}>
                {label}
            </button>
        </>
    );
};