// size
export const sm = "text-sm";
export const base = "text-base";
export const lg = "text-lg";
export const xl = "text-xl";

// weight
export const normal = "font-normal";
export const medium = "font-medium";
export const semibold = "font-semibold";
export const bold = "font-bold";

export const Typography = ({
    text = "Typography", 
    weight = normal, 
    size = base,
    className = "",
}) => {
    const style = ["font-sans", size, weight, className].join(" ");

    return (
        <span className={style}>{text}</span>
    );
};