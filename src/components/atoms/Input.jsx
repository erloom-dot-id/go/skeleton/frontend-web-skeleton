export const Input = ({
    placeholder = "",
    id = "",
    name = "",
    type = "text",
    changeHandler = (e) => {
        console.log(e.target.value);
    }, 
}) => {
    return (
        <>
            <input
                id={id}
                name={name}
                type={type}
                placeholder={placeholder}
                className="block w-full rounded-md border-0 py-1.5 px-3 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                onChange={changeHandler}
            />
        </>
    );
};