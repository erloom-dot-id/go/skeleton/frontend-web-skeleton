import { Typography, bold } from "../atoms/Typography";
import { LoginForm } from "../organisms/LoginForm";

export const LoginFormTemplate = ({
    inputEmailHandler = (e) => {
        console.log(e.target.value);
    },
    inputPasswordHandler = (e) => {
        console.log(e.target.value);
    },
    forgotPasswordHandler = () => {
        console.log("Forgot Password has Clicked");
    },
    submitHandler = () => {
        console.log("Submit has Clicked");
    }
}) => {
    return (
        <>
            <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                <img
                    className="mx-auto h-10 w-auto"
                    src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                    alt="Your Company"
                />
                <div className="mt-10 text-center">
                    <Typography
                        text="Sign in to your account"
                        weight={bold}
                        size="text-2xl"
                        className="leading-9 tracking-tight text-gray-900"
                    />
                </div>
            </div>
            <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                <LoginForm
                    inputEmailHandler={inputEmailHandler}
                    inputPasswordHandler={inputPasswordHandler}
                    forgotPasswordHandler={forgotPasswordHandler}
                    submitHandler={submitHandler}
                />
            </div>
        </>
    );
};