import React, { useEffect } from "react";
import Router from "../routes";
import { GrowthBook, GrowthBookProvider } from "@growthbook/growthbook-react";

const RouterWithGrowthbook = () => {
    const growthbook = new GrowthBook({
    apiHost: process.env.REACT_APP_GROWTHBOOK_HOST,
    clientKey: process.env.REACT_APP_GROWTHBOOK_CLIENT_KEY,
    enableDevMode: true,
    trackingCallback: (experiment, result) => {
        // TODO: Use your real analytics tracking system
        console.log("Viewed Experiment", {
            experimentId: experiment.key,
            variationId: result.key
        });
        }
    });

    useEffect(() => {
        // Load features asynchronously when the app renders
        growthbook.loadFeatures();
    }, []);

    return (
        <GrowthBookProvider growthbook={growthbook}>
          <Router/>
        </GrowthBookProvider>
    );
};

export default RouterWithGrowthbook;